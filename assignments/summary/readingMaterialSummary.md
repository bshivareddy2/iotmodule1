When these IoT capabilities are implemented in the Industrial and Manufacturing space, it becomes Industrial IoT. This technology is an amalgamation of different technologies like machine learning, big data, sensor data, M2M communication, and automation that have existed in the industrial backdrop for many years.

Industrial Internet makes a connected enterprise by merging the information and operational department of the industry. Thus improving visibility, boosting operational efficiency, increases productivity and reduces the complexity of process in the industry. Industrial IoT is a transformative manufacturing strategy that helps to improve quality, safety, productivity in an industry.

Industry 3.0 (1969)- Involved advancement of electronic technology and industrial robotics. Miniaturization of the circuit boards through programmable logic controllers, Industrial robotics to simplify, automate and increase the production. However, the operations still remained isolated from the entire enterprise.
Industry 4.0 (2010) - The vision of connected enterprise through interconnecting industrial assets through the internet was fulfilled with the introduction of Industry 4.0. The smart devices communicate with each other and create valuable insights. IIoT brought with it the advantages of asset optimization, production integration, smart monitoring, remote diagnosis, intelligent decision making and most importantly the feature of Predictive Maintenance.
1st industrial revolution – Steam and water power are used to mechanize production
2nd industrial revolution – Electricity allows for mass production with assembly lines
3rd industrial revolution – IT and computer technology are used to automate processes
4th industrial revolution (Industry 4.0) – Enhancing automation and connectivity with CPS


What is the difference between Industry 3.0 and Industry 4.0?
In Industry 3.0, we automate processes using logic processors and information technology. These processes often operate largely without human interference, but there is still a human aspect behind it. Where Industry 4.0 comes in is with the availability and use of vast quantities of data on the production floor.
The goal of Industry 4.0 is “The Intelligent Factory”, which is characterized by adaptability, resource efficiency and ergonomics as well as the integration of customers and business partners in business and value processes. This “Factory of the Future”, will regard everything as a service (including tools and skills), it will require supply-chain integration and data availability. These combined elements will allow for the integration of the entire production process and supply chain and should, in time, enable the self-optimization of cyber-based physical systems 



